# Sistema PPC

Sistema de software para gestão do Plano Pedagógico de Cursos.
Projeto Integrador 3 - Grupo 2 - Curso de Análise e Desenvolvimento de Sistemas | IESB

## Introdução

Aplicação Web para gestão do Plano Pedagógico de Cursos de uma instituição de ensino superior.

### Pré-requisitos

```
O usuário deve estar previamente cadastrado na instituição de ensino e possuir uma matricula.
```

```
Aplicação executada em Java 1.8+.
```

### Instalação

```
Fazer o deploy do arquivo .war e suas depedências no servidor web.
```

## Rodando os testes

Os testes são executados pelo Maven sempre que é executado uma ação de deploy.

## Construido com

* [Java 8] (http://www.oracle.com/technetwork/pt/java/javaee/overview/index.html)
* [Spring MVC] (https://spring.io/)
* [Maven] (https://maven.apache.org/)
* [HTML 5] (https://www.w3schools.com/html/html5_intro.asp)
* [BootStrap 3.3.7] (http://getbootstrap.com/)
* [JavaScript] (https://www.javascript.com/)
* [PostgreSql] (https://www.postgresql.org/)

## Autores

* **Marks Duarte** - [MarksDuarte](https://gitlab.com/marksduarte) - Desenvolvimento Backend e Frontend
* **Kalinin Filho** - [Kalinin Filho] - Testes de Funcionalidade
* **Iria Luna** - [IriaLuna] (https://gitlab.com/irialuna) - Testes de Funcionalidade
* **Vinicio Eto** - [VinicioEto] (https://gitlab.com/vinicioeto) - Desenvolvimento Frontend

## Licença

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


