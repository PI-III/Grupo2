package com.ads.semestre3.ppc.interceptor;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Component
public class RequestInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object object) throws Exception {
        String requestURI = request.getRequestURI();
        if(requestURI.endsWith("login") || requestURI.endsWith("cadastro") || requestURI.endsWith("/") || requestURI.contains("resources")){
            return true;
        }
        if(request.getSession().getAttribute("usuarioLogado") != null) {
            return true;
        }
        response.sendRedirect("/usuario/login");
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
                           Object object, ModelAndView model)
            throws Exception {
        //System.out.println("PostHandle");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
                                Object object, Exception arg3)
            throws Exception {
        //System.out.println("afterCompletion");
    }
}
