package com.ads.semestre3.ppc.repository;

import com.ads.semestre3.ppc.model.Coordenador;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Coordenadores extends JpaRepository<Coordenador, Long> {
}
