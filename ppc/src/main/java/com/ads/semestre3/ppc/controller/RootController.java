package com.ads.semestre3.ppc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author marks duarte
 */

@Controller
@RequestMapping
public class RootController {

    @RequestMapping("/home")
    public ModelAndView home() {
        return new ModelAndView("Home");
    }



}
