package com.ads.semestre3.ppc.repository;

import com.ads.semestre3.ppc.model.PlanoDeEnsino;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlanoDeEnsinos extends JpaRepository<PlanoDeEnsino, Long> {
}

