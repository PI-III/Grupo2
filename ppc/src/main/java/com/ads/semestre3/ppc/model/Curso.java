package com.ads.semestre3.ppc.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "curso")
public class Curso {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    private Coordenador coordenador;

    /**
     * MappedBy: informamos o nome da variável de instância que vai indicar a quem aquele One pertence;
     * TargetEntity: informa qual a entidade estamos associando;
     * FetchType.Lazy: foi escolhido por performace;
     * cascade: ALL para permitir alterações em todos os relacionamentos;
     */
    @OneToOne(mappedBy = "curso", targetEntity = Disciplina.class,
            fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    /**
     * A anotação @JoinTable indica que estamos interagindo com uma tabela intermediária;
     * A anotação @JoinColumns é resposnável pelo mapeamento de colunas do lado que é o dono;
     * O atributo name possui o nome da coluna da tabela intermediária;
     * referencedColumnName contém o nome da coluna chave-primária do lado que é dono;
     * O atributo inverseJoinColumns é responsável por mapear colunas do lado inverso.
     */
    @JoinTable(name = "curso_disciplina",
            joinColumns ={@JoinColumn(name = "curso_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "disciplina_id", referencedColumnName = "id")})
    private List<Disciplina> disciplinas;

    @NotNull(message = "O campo tipo é obrigatório.")
    private String tipo;
    private String modalidade;
    private String denominacao;
    private String habilitacao;
    private String localOferta;

    @OneToOne(mappedBy = "curso", targetEntity = Turno.class,
            fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "curso_turno",
            joinColumns ={@JoinColumn(name = "curso_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "turno_id", referencedColumnName = "id")})
    private List<Turno> turnos;

    private Integer qntVagas;
    private Integer cargaHoraria;
    private String regimeLetivo;
    private String periodo;

    public Curso() {
    }

    public Long getId() {
        return id;
    }

    public Coordenador getCoordenador() {
        return coordenador;
    }

    public void setCoordenador(Coordenador coordenador) {
        this.coordenador = coordenador;
    }

    public List<Disciplina> getDisciplinas() {
        return disciplinas;
    }

    public void setDisciplinas(List<Disciplina> disciplinas) {
        this.disciplinas = disciplinas;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getModalidade() {
        return modalidade;
    }

    public void setModalidade(String modalidade) {
        this.modalidade = modalidade;
    }

    public String getDenominacao() {
        return denominacao;
    }

    public void setDenominacao(String denominacao) {
        this.denominacao = denominacao;
    }

    public String getHabilitacao() {
        return habilitacao;
    }

    public void setHabilitacao(String habilitacao) {
        this.habilitacao = habilitacao;
    }

    public String getLocalOferta() {
        return localOferta;
    }

    public void setLocalOferta(String localOferta) {
        this.localOferta = localOferta;
    }

    public List<Turno> getTurnos() {
        return turnos;
    }

    public void setTurnos(List<Turno> turnos) {
        this.turnos = turnos;
    }

    public Integer getQntVagas() {
        return qntVagas;
    }

    public void setQntVagas(Integer qntVagas) {
        this.qntVagas = qntVagas;
    }

    public Integer getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(Integer cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public String getRegimeLetivo() {
        return regimeLetivo;
    }

    public void setRegimeLetivo(String regimeLetivo) {
        this.regimeLetivo = regimeLetivo;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }
}
