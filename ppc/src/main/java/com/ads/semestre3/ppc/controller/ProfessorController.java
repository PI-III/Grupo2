package com.ads.semestre3.ppc.controller;

import com.ads.semestre3.ppc.model.Disciplina;
import com.ads.semestre3.ppc.model.IesAtuacaoProfissional;
import com.ads.semestre3.ppc.model.IesGeral;
import com.ads.semestre3.ppc.model.Professor;
import com.ads.semestre3.ppc.repository.Disciplinas;
import com.ads.semestre3.ppc.repository.IesGerais;
import com.ads.semestre3.ppc.repository.PlanoDeEnsinos;
import com.ads.semestre3.ppc.repository.Professores;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Arrays;
import java.util.List;

/**
 * @author marks duarte
 */

@Controller
@RequestMapping("/professor")
public class ProfessorController {

    @Autowired
    private Professores professores;

    @Autowired
    private IesGerais iesGerais;

    @Autowired
    private Disciplinas disciplinas;

    @RequestMapping("/cadastro/pessoal")
    public ModelAndView dadosPessoais() {
        ModelAndView mv = new ModelAndView("CadastroProfessorPessoal");
        mv.addObject(new Professor()); //Adiciona o objeto professor à view para o thymeleaf utilizar no formulário
        return mv;
    }

    @RequestMapping("/cadastro/geral")
    public ModelAndView dadosGerais() {
        ModelAndView mv = new ModelAndView("CadastroProfessorGeral");
        mv.addObject(new IesGeral());
        return mv;
    }

    @RequestMapping("/cadastro/atuacao")
    public ModelAndView dadosAtuacao() {
        ModelAndView mv = new ModelAndView("CadastroProfessorAtuacao");
        mv.addObject(new IesAtuacaoProfissional());
        return mv;
    }

    @RequestMapping("/cadastro/publicacao")
    public ModelAndView dadosPublicacao() {
        ModelAndView mv = new ModelAndView("CadastroProfessorPublicacao");
        return mv;
    }

    @RequestMapping(value = "/cadastro/pessoal", method = RequestMethod.POST)
    public String cadastraDadosPessoais(@Validated Professor professor, Errors errors, RedirectAttributes attributes) {
        if(errors.hasErrors()){
            return "CadastroProfessorPessoal";
        }
        for(Professor prof : professores.findAll()){
            if(prof.getCpf().equals(professor.getCpf())){
                attributes.addFlashAttribute("mensagem","O professor " + prof.getNome()
                        + " já está cadastrado com o CPF: " + professor.getCpf());
                return "redirect:/professor/cadastro/pessoal";
            }
        }
        professores.save(professor);
        attributes.addFlashAttribute("mensagem","Dados pessoais cadastrados com sucesso!");
        return "redirect:/professor/cadastro/pessoal";
    }

    @RequestMapping(value = "/cadastro/geral", method = RequestMethod.POST)
    public String cadastraDadosGerais(@Validated IesGeral iesGeral, Errors errors, RedirectAttributes attributes){
        if(errors.hasErrors()){
            return "CadastroProfessorGeral";
        }
        iesGerais.save(iesGeral);
        attributes.addFlashAttribute("mensagem","Dados cadastrados com sucesso!");
        return "redirect:/professor/cadastro/geral";
    }

    @RequestMapping("/pesquisa")
    public ModelAndView pesquisaProfessor(){
        ModelAndView mv = new ModelAndView("PesquisaProfessor");
        mv.addObject("todosProfessores", listaProfessores());
        return mv;
    }

    @ModelAttribute("todosProfessores")
    public List<Professor> listaProfessores(){
        return professores.findAll();
    }

    @ModelAttribute("disciplinasProfessor")
    public List<Disciplina> disciplinasProfessor(){
        return disciplinas.findAll();
    }

}
