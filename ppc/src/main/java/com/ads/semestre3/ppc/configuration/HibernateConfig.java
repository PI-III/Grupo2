package com.ads.semestre3.ppc.configuration;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URI;
import java.net.URISyntaxException;

@Configuration
public class HibernateConfig {
    /**
     * Database configuration for MySql Heroku
     * @return
     * @throws URISyntaxException
     */
//    @Bean
//    public DataSource dataSource() throws URISyntaxException {
//        URI dbUri = new URI(System.getenv("CLEARDB_DATABASE_URL"));
//
//        String username = dbUri.getUserInfo().split(":")[0];
//        String password = dbUri.getUserInfo().split(":")[1];
//        String dbUrl = "jdbc:mysql://" + dbUri.getHost() + dbUri.getPath() + "?reconnect=true";
//
//        DataSource basicDataSource = new DataSource();
//        basicDataSource.setUrl(dbUrl);
//        basicDataSource.setUsername(username);
//        basicDataSource.setPassword(password);
//
//        return basicDataSource;
//    }

        /**
     * Database configuration for Heroku PostGreSql
     * @return
     * @throws URISyntaxException
     */
//    @Bean
//    public DataSource dataSource() throws URISyntaxException {
//        URI dbUri = new URI(System.getenv("DATABASE_URL"));
//
//        String username = dbUri.getUserInfo().split(":")[0];
//        String password = dbUri.getUserInfo().split(":")[1];
//        String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath();
//
//        System.out.println("DATBASE_URL ------> " + dbUrl);
//
//        DataSource basicDataSource = new DataSource();
//        basicDataSource.setUrl(dbUrl);
//        basicDataSource.setUsername(username);
//        basicDataSource.setPassword(password);
//
//        return basicDataSource;
//    }

    /**
     * Database configuration for postgresql localhost
     * @return
     * @throws URISyntaxException
     */
    @Bean
    public DataSource dataSource() throws URISyntaxException {
        //URI dbUri = new URI(System.getenv("DATABASE_URL"));

        String username = "postgres";
        String password = "m@trix23";
        String dbUrl = "jdbc:postgresql://localhost:5432/ppc";

        DataSource basicDataSource = new DataSource();
        basicDataSource.setUrl(dbUrl);
        basicDataSource.setUsername(username);
        basicDataSource.setPassword(password);

        return basicDataSource;
    }

}
