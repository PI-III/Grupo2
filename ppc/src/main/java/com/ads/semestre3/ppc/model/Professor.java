package com.ads.semestre3.ppc.model;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.Date;

@Entity
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinTable(name = "disciplina_professor",
            joinColumns = {@JoinColumn(name = "disciplina_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "professor_id", referencedColumnName = "id")})
    private Disciplina disciplina;

    @NotEmpty(message = "O campo Nome é obrigatório!")
    @Size(max = 60, message = "O campo Nome não aceita mais de 60 caracteres.")
    private String nome;

    @NotEmpty(message = "O campo CPF é obrigatório!")
    private String cpf;

    @NotEmpty(message = "O campo Maior Titulação é obrigatório!")
    private String maiorTitulacao;

    @NotEmpty(message = "O campo Área de Formação da Maior Titulação é obrigatório!")
    private String areaFormacao;

    @NotEmpty(message = "O campo Curriculo Lates é obrigatório!")
    private String urlCurriculo;

    @NotNull(message = "O campo Data de Atualização do Curriculo Lates é obrigatório!")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.DATE)
    private Date dataAtualizacao;

    @NotNull
    private String matricula;

    public Professor() {
        this.matricula = "iesb-" + this.id;
    }

    public Long getId() {
        return id;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getMaiorTitulacao() {
        return maiorTitulacao;
    }

    public void setMaiorTitulacao(String maiorTitulacao) {
        this.maiorTitulacao = maiorTitulacao;
    }

    public String getAreaFormacao() {
        return areaFormacao;
    }

    public void setAreaFormacao(String areaFormacao) {
        this.areaFormacao = areaFormacao;
    }

    public String getUrlCurriculo() {
        return urlCurriculo;
    }

    public void setUrlCurriculo(String urlCurriculo) {
        this.urlCurriculo = urlCurriculo;
    }

    public Date getDataAtualizacao() {
        return dataAtualizacao;
    }

    public void setDataAtualizacao(Date dataAtualizacao) {
        this.dataAtualizacao = dataAtualizacao;
    }

    public String getMatricula() {
        return matricula;
    }

    @Override
    public String toString() {
        return nome;
    }
}
