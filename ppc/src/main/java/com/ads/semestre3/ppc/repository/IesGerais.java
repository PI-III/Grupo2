package com.ads.semestre3.ppc.repository;

import com.ads.semestre3.ppc.model.IesGeral;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IesGerais extends JpaRepository<IesGeral, Long> {
}
