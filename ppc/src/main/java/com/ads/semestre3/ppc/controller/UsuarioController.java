package com.ads.semestre3.ppc.controller;

import com.ads.semestre3.ppc.model.Professor;
import com.ads.semestre3.ppc.model.Usuario;
import com.ads.semestre3.ppc.repository.Professores;
import com.ads.semestre3.ppc.repository.Usuarios;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Parameter;
import java.util.List;

/**
 * @author marks duarte
 */

@Controller
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private Usuarios usuarios;

    @Autowired
    private Professores professores;

    @RequestMapping("/login")
    public ModelAndView login() {
        return new ModelAndView("Login");
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ModelAndView acessarSistema(WebRequest request, HttpSession session) {
        Usuario usuarioEncontrado = null;
        ModelAndView mv = new ModelAndView();

        for (Usuario u : usuarios.findAll()) {
            if (u.getMatricula().equals(request.getParameter("matricula")) && u.getPassword().equals(request.getParameter("password"))) {
                usuarioEncontrado = u;
            }
        }

        if (usuarioEncontrado != null) {
            session.setAttribute("usuarioLogado", usuarioEncontrado.getMatricula());
            mv.setViewName("Home");
            System.out.println("Usuário logado: " + usuarioEncontrado.getMatricula());
            return mv;
        } else {
            mv.setViewName("Login");
            mv.addObject("mensagemErro", "Nome de usuário ou senha inválido.");
            return mv;
        }
    }

    @RequestMapping(value = "/cadastro", method = RequestMethod.POST)
    public ModelAndView novoUsuario(Usuario usuario, WebRequest request){
        ModelAndView mv = new ModelAndView();
        System.out.println("---------------------> " + request.getParameter("matricula"));

        for(Professor p : professores.findAll()){
            System.out.println( "---------------------> " + p.getMatricula());
            if(p.getMatricula().equals(request.getParameter("matricula"))){
                if(usuario.getPassword().equals(request.getParameter("confirmPassword"))){
                    usuario.setProfessor(p);
                    usuarios.save(usuario);
                    mv.setViewName("Login");
                    mv.addObject("mensagemSucesso", "Usuário cadastrado com sucesso!");
                } else {
                    mv.setViewName("Login");
                    mv.addObject("mensagemErro","Erro ao cadastrar usuário.");
                }
                break;
            }
        }

        return mv;
    }

    @RequestMapping(value = "/logout")
    public ModelAndView logout(HttpSession session){
        session.invalidate();
        return new ModelAndView("Login");
    }
}
