package com.ads.semestre3.ppc.model;

public enum CargaHoraria {
    CARGA30("30 Horas"),
    CARGA60("60 Horas"),
    CARGA90("90 Horas"),
    CARGA120("120 Horas");

    private String descricao;

    CargaHoraria(String descricao){
        this.descricao = descricao;
    }

    public String getDescricao(){
        return descricao;
    }
}
