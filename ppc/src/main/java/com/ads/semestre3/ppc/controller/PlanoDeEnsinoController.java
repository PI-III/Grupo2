package com.ads.semestre3.ppc.controller;

import com.ads.semestre3.ppc.model.PlanoDeEnsino;
import com.ads.semestre3.ppc.repository.Professores;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
/**
 * @author marks duarte
 */

@Controller
@RequestMapping("/planosdeensino")
public class PlanoDeEnsinoController {

    @Autowired
    private Professores professores;

    @RequestMapping("/cadastro")
    public ModelAndView cadastroPlanoEnsino() {
        ModelAndView mv = new ModelAndView("CadastroPlanoDeEnsino");
        mv.addObject(new PlanoDeEnsino());
        return mv;
    }

}
