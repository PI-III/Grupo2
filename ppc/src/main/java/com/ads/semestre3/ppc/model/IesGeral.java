package com.ads.semestre3.ppc.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class IesGeral {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @OneToOne
    private Professor professor;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Temporal(TemporalType.DATE)
    private Date dataAdmissao;

    private Integer horasNde;
    private Integer qntOrientacaoTcc;
    private Integer qntCoordenacaoCurso;
    private Integer qntCoordenacaoOutrosCursos;
    private Integer qntPesquisaSemestreAtual;
    private Integer qntAtividadeExtraCurso;
    private Integer qntAtividadeExtraOutrosCursos;
    private Integer qntHorasCurso;
    private Integer qntHorasOutrosCursos;

    public IesGeral() {
    }

    public Long getId() {
        return id;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Date getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(Date dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public Integer getHorasNde() {
        return horasNde;
    }

    public void setHorasNde(Integer horasNde) {
        this.horasNde = horasNde;
    }

    public Integer getQntOrientacaoTcc() {
        return qntOrientacaoTcc;
    }

    public void setQntOrientacaoTcc(Integer qntOrientacaoTcc) {
        this.qntOrientacaoTcc = qntOrientacaoTcc;
    }

    public Integer getQntCoordenacaoCurso() {
        return qntCoordenacaoCurso;
    }

    public void setQntCoordenacaoCurso(Integer qntCoordenacaoCurso) {
        this.qntCoordenacaoCurso = qntCoordenacaoCurso;
    }

    public Integer getQntCoordenacaoOutrosCursos() {
        return qntCoordenacaoOutrosCursos;
    }

    public void setQntCoordenacaoOutrosCursos(Integer qntCoordenacaoOutrosCursos) {
        this.qntCoordenacaoOutrosCursos = qntCoordenacaoOutrosCursos;
    }

    public Integer getQntPesquisaSemestreAtual() {
        return qntPesquisaSemestreAtual;
    }

    public void setQntPesquisaSemestreAtual(Integer qntPesquisaSemestreAtual) {
        this.qntPesquisaSemestreAtual = qntPesquisaSemestreAtual;
    }

    public Integer getQntAtividadeExtraCurso() {
        return qntAtividadeExtraCurso;
    }

    public void setQntAtividadeExtraCurso(Integer qntAtividadeExtraCurso) {
        this.qntAtividadeExtraCurso = qntAtividadeExtraCurso;
    }

    public Integer getQntAtividadeExtraOutrosCursos() {
        return qntAtividadeExtraOutrosCursos;
    }

    public void setQntAtividadeExtraOutrosCursos(Integer qntAtividadeExtraOutrosCursos) {
        this.qntAtividadeExtraOutrosCursos = qntAtividadeExtraOutrosCursos;
    }

    public Integer getQntHorasCurso() {
        return qntHorasCurso;
    }

    public void setQntHorasCurso(Integer qntHorasCurso) {
        this.qntHorasCurso = qntHorasCurso;
    }

    public Integer getQntHorasOutrosCursos() {
        return qntHorasOutrosCursos;
    }

    public void setQntHorasOutrosCursos(Integer qntHorasOutrosCursos) {
        this.qntHorasOutrosCursos = qntHorasOutrosCursos;
    }


}
