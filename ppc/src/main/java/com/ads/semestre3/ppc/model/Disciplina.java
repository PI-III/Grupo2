package com.ads.semestre3.ppc.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Disciplina {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinTable(name = "curso_disciplina",
            joinColumns ={@JoinColumn(name = "curso_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "disciplina_id", referencedColumnName = "id")})
    private Curso curso;

    @OneToOne(mappedBy = "disciplina", targetEntity = Professor.class,
            fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "disciplina_professor",
            joinColumns ={@JoinColumn(name = "disciplina_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "professor_id", referencedColumnName = "id")})
    private List<Professor> professores;

    private String nome;
    private String descricao;
    private String codigo;
    private String semestre;

    @Enumerated(EnumType.STRING)
    private CargaHoraria cargaHoraria;

    public Disciplina() {

    }

    public Long getId() {
        return id;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public List<Professor> getProfessores() {
        return professores;
    }

    public void setProfessores(List<Professor> professores) {
        this.professores = professores;
    }

    public String getNome() {
        return nome;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCargaHoraria() {
        return cargaHoraria.getDescricao();
    }

    public void setCargaHoraria(CargaHoraria cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    @Override
    public String toString() {
        return "Disciplina{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", descricao='" + descricao + '\'' +
                ", codigo='" + codigo + '\'' +
                ", semestre='" + semestre + '\'' +
                ", cargaHoraria='" + cargaHoraria + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Disciplina that = (Disciplina) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}

