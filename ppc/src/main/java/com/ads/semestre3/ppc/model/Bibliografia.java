package com.ads.semestre3.ppc.model;

import javax.persistence.*;

//@Entity
public class Bibliografia {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Curso curso;

    public Bibliografia() {
    }

    public Long getId() {
        return id;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }
}
