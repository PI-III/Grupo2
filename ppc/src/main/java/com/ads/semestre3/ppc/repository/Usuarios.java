package com.ads.semestre3.ppc.repository;

import com.ads.semestre3.ppc.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Usuarios extends JpaRepository<Usuario, Long> {
}
