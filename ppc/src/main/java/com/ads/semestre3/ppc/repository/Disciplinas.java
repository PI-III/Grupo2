package com.ads.semestre3.ppc.repository;

import com.ads.semestre3.ppc.model.Disciplina;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Disciplinas extends JpaRepository<Disciplina, Long> {
}

