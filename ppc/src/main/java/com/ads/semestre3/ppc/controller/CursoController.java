package com.ads.semestre3.ppc.controller;

import com.ads.semestre3.ppc.model.Coordenador;
import com.ads.semestre3.ppc.model.Curso;
import com.ads.semestre3.ppc.repository.Coordenadores;
import com.ads.semestre3.ppc.repository.Cursos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * @author marks duarte
 */

@Controller
@RequestMapping("/curso")
public class CursoController {

    private static final String CADASTRO_CURSO = "CadastroCurso";
    private static final String PESQUISA_CURSO = "PesquisaCurso";

    @Autowired
    private Cursos cursos;

    @Autowired
    private Coordenadores coordenadores;

    @RequestMapping("/cadastro")
    public ModelAndView curso(){
        ModelAndView mv = new ModelAndView(CADASTRO_CURSO);
        mv.addObject(new Curso());
        mv.addObject(new Coordenador());
        mv.addObject("todosCoordenadores", coordenadores.findAll());
        return mv;
    }

    @RequestMapping("/pesquisa")
    public ModelAndView pesquisa(){
        ModelAndView mv = new ModelAndView(PESQUISA_CURSO);
        mv.addObject(new Curso());
        return mv;
    }

    @RequestMapping(value = "/pesquisa/{id}")
    public ModelAndView editaCurso(@PathVariable("id") Curso curso){
        ModelAndView mv = new ModelAndView(CADASTRO_CURSO);
        mv.addObject(new Coordenador());
        mv.addObject(curso);
        return mv;
    }

    @RequestMapping(value = "/cadastro", method = RequestMethod.POST)
    public String cadastroCurso(@Validated Curso curso, Errors errors, RedirectAttributes attributes){
        if(errors.hasErrors()){
            return CADASTRO_CURSO;
        }
        cursos.save(curso);
        attributes.addFlashAttribute("mensagem","Curso cadastrado com sucesso!");
        return "redirect:/curso/cadastro";
    }

   @ModelAttribute("todosCursos")
    public List<Curso> todosCursos(){
        return cursos.findAll();
   }

}
