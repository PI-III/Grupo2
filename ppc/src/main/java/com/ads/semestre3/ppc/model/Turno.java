package com.ads.semestre3.ppc.model;

import javax.persistence.*;

@Entity
public class Turno {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinTable(name = "curso_turno",
            joinColumns ={@JoinColumn(name = "curso_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "turno_id", referencedColumnName = "id")})
    private Curso curso;

    private String turnoDia;
    private Integer qntVagas;

    public Turno() {
    }

    public Long getId() {
        return id;
    }

    public String getTurnoDia() {
        return turnoDia;
    }

    public void setTurnoDia(String turnoDia) {
        this.turnoDia = turnoDia;
    }

    public Integer getQntVagas() {
        return qntVagas;
    }

    public void setQntVagas(Integer qntVagas) {
        this.qntVagas = qntVagas;
    }
}
