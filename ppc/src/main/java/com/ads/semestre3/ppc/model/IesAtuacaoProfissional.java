package com.ads.semestre3.ppc.model;

import javax.persistence.*;
import java.util.Date;

//@Entity
public class IesAtuacaoProfissional {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private IesGeral iesGeral;

    private Boolean membroNde;
    private Boolean membroColegiado;
    private Boolean formacaoPedagogica;
    private Date dtDocenteCurso;
    private Date dtExpMagisterio;
    private Date dtExpEad;
    private Date dtExpProfissao;

    public IesAtuacaoProfissional() {
    }

    public Long getId() {
        return id;
    }

    public IesGeral getIesGeral() {
        return iesGeral;
    }

    public void setIesGeral(IesGeral iesGeral) {
        this.iesGeral = iesGeral;
    }

    public Boolean getMembroNde() {
        return membroNde;
    }

    public void setMembroNde(Boolean membroNde) {
        this.membroNde = membroNde;
    }

    public Boolean getMembroColegiado() {
        return membroColegiado;
    }

    public void setMembroColegiado(Boolean membroColegiado) {
        this.membroColegiado = membroColegiado;
    }

    public Boolean getFormacaoPedagogica() {
        return formacaoPedagogica;
    }

    public void setFormacaoPedagogica(Boolean formacaoPedagogica) {
        this.formacaoPedagogica = formacaoPedagogica;
    }

    public Date getDtDocenteCurso() {
        return dtDocenteCurso;
    }

    public void setDtDocenteCurso(Date dtDocenteCurso) {
        this.dtDocenteCurso = dtDocenteCurso;
    }

    public Date getDtExpMagisterio() {
        return dtExpMagisterio;
    }

    public void setDtExpMagisterio(Date dtExpMagisterio) {
        this.dtExpMagisterio = dtExpMagisterio;
    }

    public Date getDtExpEad() {
        return dtExpEad;
    }

    public void setDtExpEad(Date dtExpEad) {
        this.dtExpEad = dtExpEad;
    }

    public Date getDtExpProfissao() {
        return dtExpProfissao;
    }

    public void setDtExpProfissao(Date dtExpProfissao) {
        this.dtExpProfissao = dtExpProfissao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IesAtuacaoProfissional that = (IesAtuacaoProfissional) o;

        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
