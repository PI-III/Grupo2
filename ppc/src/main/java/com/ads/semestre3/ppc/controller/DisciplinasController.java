package com.ads.semestre3.ppc.controller;

import com.ads.semestre3.ppc.model.CargaHoraria;
import com.ads.semestre3.ppc.model.Disciplina;
import com.ads.semestre3.ppc.repository.Disciplinas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Arrays;
import java.util.List;

/**
 * @author marks duarte
 */

@Controller
@RequestMapping("/disciplina")
public class DisciplinasController {

    private static final String CADASTRO_DISCIPLINA = "CadastroDisciplina";
    private static final String PESQUISA_DISCIPLINA = "PesquisaDisciplina";

    @Autowired
    private Disciplinas disciplinas;

    @RequestMapping("/cadastro")
    public ModelAndView nova() {
        ModelAndView mv = new ModelAndView(CADASTRO_DISCIPLINA);
        mv.addObject(new Disciplina());
        return mv;
    }

    @RequestMapping("/pesquisa")
    public ModelAndView pesquisa() {
        ModelAndView mv = new ModelAndView(PESQUISA_DISCIPLINA);
        mv.addObject(new Disciplina());
        return mv;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView salvar(Disciplina disciplina) {
        disciplinas.save(disciplina);
        ModelAndView mv = new ModelAndView(CADASTRO_DISCIPLINA);
        mv.addObject("mensagem", "Disciplina cadastrada com sucesso!");
        return mv;
    }

    @RequestMapping(value = "/pesquisa", method = RequestMethod.POST)
    public ModelAndView pesquisaDisciplina(){
        ModelAndView mv = new ModelAndView(PESQUISA_DISCIPLINA);
        mv.addObject("todasDisciplinas", todasDisciplinas());
        return mv;
    }

    @RequestMapping(value = "/pesquisa/{id}")
    public ModelAndView editaDisciplina(@PathVariable("id") Disciplina disciplina){
        ModelAndView mv = new ModelAndView(CADASTRO_DISCIPLINA);
        mv.addObject(disciplina);
        return mv;
    }

    @ModelAttribute("todasCargasHorarias")
    public List<CargaHoraria> todasCargasHorarias() {
        return Arrays.asList(CargaHoraria.values());
    }

    @ModelAttribute("todasDisciplinas")
    public List<Disciplina> todasDisciplinas(){
        return disciplinas.findAll();
    }

}
