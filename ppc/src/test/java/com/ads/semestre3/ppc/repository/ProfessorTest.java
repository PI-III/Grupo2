package com.ads.semestre3.ppc.repository;

import com.ads.semestre3.ppc.SistemaPpcApplicationTests;
import com.ads.semestre3.ppc.controller.ProfessorController;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

/**
 * @author marks duarte
 */
public class ProfessorTest extends SistemaPpcApplicationTests {

    private MockMvc mockMvc;

    @Autowired
    private ProfessorController professorController;

    @Before
    public void setUp(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(professorController).build();
    }

    @Test
    public void testGETIndexProfessorController() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/professor/cadastro/pessoal")).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testPOSTCadastroProfessorPessoalController() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/professor/cadastro/pessoal")
                .param("nome", "Professor Teste")
                .param("cpf", "12345678910")
                .param("maiorTitulacao", "Maior Titulação")
                .param("areaFormacao", "Área de Formação")
                .param("urlCurriculo", "http://url-curriculo")
                .param("dataAtualizacao", "23/11/2017")
        ).andExpect(MockMvcResultMatchers.redirectedUrl("/professor/cadastro/pessoal"));
    }
}